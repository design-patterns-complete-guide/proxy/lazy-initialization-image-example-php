<?php
interface  Image
{
    public function display();
}
class RealImage implements Image
{
    private $filename;
    public function __construct($filename)
    {
        $this->filename = $filename;
        $this->loadImageFromDisk();
    }
    private function loadImageFromDisk()
    {
        echo "Loading image from disk: {$this->filename}\n";
    }
    public function display()
    {
        echo "Displaying image: {$this->filename}\n";
    }
}
class ProxyImage implements Image
{
    private $realImage;
    private $filename;
    public function __construct($filename)
    {
        $this->filename = $filename;
    }
    public function display()
    {
        if ($this->realImage === null) {
            $this->realImage = new RealImage($this->filename);
        }
        $this->realImage->display();
    }
}
// Client code
$image1 = new ProxyImage("image1.jpg");
$image2 = new ProxyImage("image2.jpg");
$image1->display(); // This will load and display "image1.jpg"
$image2->display(); // This will load and display "image2.jpg"
$image1->display(); //this won't load it will display "image1.jpg"